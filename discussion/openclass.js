let students = [];
let sectionedStudents = [];


function addStudent(name) {

	// call out the array and use the push() to add a new element inside the container.
	students.push(name);
	console.log(name + ' has been added to the list.');

}

addStudent('Rem');
addStudent('Wanda');
addStudent('Xylar');
addStudent('Abier');
console.log(students);

function countStudents() {
	//display the number of elements inside the array with the prescribed message.
	console.log('This class has a total of ' + students.length + ' enrolled students.');
}
countStudents();

function printStudents() {
	//sort the elements inside the array in alphabetical order.
	students = students.sort();
	console.log(students);

	students.forEach(function(student) {
		console.log(student);
	})
}

printStudents();

function findStudent(keyword) {
	let matches = students.filter(function(student) {
		// keyword = keyword.toLowerCase()
		return	student.toLowerCase().includes(keyword.toLowerCase());
	})
	// console.log(matches);
	// create a control structure that will give the proper response according to the result of the filter.
	if (matches.length === 1) {
		console.log(matches[0] + ' is enrolled')
	} else if (matches.length > 1) {
		console.log('Multiple student matches this keyword')
	}
	else {
		console.log('No student matches this keyword')
	}
}

findStudent('a');

function addSection(section) {
	sectionStudents = students.map(function(student) {
		return student + ' is part of section ' + section;
	})

	console.log(sectionStudents);
	// map() - will create a new array populated with the elements that passes a condition on a given function/argument.
}
addSection('4');

function removeStudent(name) {
	let result = students.indexOf(name);
	// console.log(result)
	if (result >= 0) {
		// We will remove the element using splice method.
		students.splice(result, 1)
		console.log(students);
		console.log(name + ' has been removed');	
	} else {
		console.log('No student was removed.');
	}
	
}
removeStudent('Abier');
// console.log(students.indexOf('Rem')); //This will return the index number of a certain element.